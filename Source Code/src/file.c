#include "extend.h"

	#ifdef __cplusplus
	extern "C" {
	#endif
	char * file_read(char * _arg0)
	{
		int i = 1;
		int c;
		char * result = NULL;
		
		FILE * fp;
		if(!(fp = fopen(_arg0, "rb")))return NULL;
		while(1)
		{
			c=fgetc(fp);
			if(feof(fp))break;
			result = (char*)realloc(result, i*sizeof(char));
			*(result + (i-1)) = c;
			i++;
		}
		result = (char*)realloc(result, i*sizeof(char));
		*(result + (i-1)) = '\0';
		if (fclose(fp) == -1) return NULL;
		return result;
	}
	
	int file_write(char * _arg0, char * _arg1)
	{
		FILE *file;
		file = fopen (_arg0, "w");
		if(file==NULL)return -1;
		fprintf(file, "%s",_arg1); 
		fclose(file);
		return 0;
	}
	
	int file_exists(char * _arg0)
	{
		FILE *file;
		if (!(file = fopen(_arg0, "r")))return -1;
		fclose(file);
		return 0;
	}
	
	int file_append(char * _arg0, char * _arg1)
	{
		FILE *file;
		if(file_exists(_arg0)==-1)
			if(file_create(_arg0)==-1)return -1;
		file = fopen (_arg0, "a");
		if(file==NULL)return -1;
		fprintf(file, "%s",_arg1); 
		fclose(file);
		return 0;
	}
	
	int file_create(char * _arg0)
	{
		FILE *file;
		file = fopen(_arg0, "wb");
		if(file==NULL)return -1;
		return 0;
	}
	
	int file_delete(char * _arg0)
	{
		if(file_exists(_arg0)==-1)return -2;	
		return remove(_arg0);
	}
#ifdef __cplusplus
}
#endif
