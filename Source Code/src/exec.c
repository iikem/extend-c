#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "extend.h"

	char * command_exec(char * _arg0)
	{
		int i = 1;
		int c;
		char * result = NULL;
		FILE * fp;
		if(!(fp = popen(_arg0, "r")))return NULL;
		while(1)
		{
			c=fgetc(fp);
			if(feof(fp))break;
			if( (result = (char*)realloc(result, i*sizeof(char))) == NULL ) return NULL;
			*(result + (i-1)) = c;
			i++;
		}
		if( (result = (char*)realloc(result, i*sizeof(char))) == NULL ) return NULL;;
		*(result + (i-1)) = '\0';
		pclose(fp);
		return result;
	}
