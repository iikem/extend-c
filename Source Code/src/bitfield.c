#include <stdio.h>
#include <stdlib.h>
#include "extend.h"

	void bitfield_print(bitfield * bits)
	{
		bitfield mask = 1,i;
		for (i=0;i<sizeof(bitfield)*4;i++)
		{
			(mask & *bits) ? printf("1"):printf("0");
			mask = mask << 1;
		}
	}

	void bitfield_bit_change(bitfield * bitstream,int position,int value)
	{
		( * bitstream ) ^= (-value ^ ( * bitstream )) & (1 << position);
	}

	bitfield bitfield_bit_retrieve(bitfield * bitstream,int position)
	{
		return (bitfield)((( * bitstream ) >> position) & 1);
	}
	
	void bitfield_bit_flip(bitfield * bitstream,int position)
	{
		( * bitstream ) ^= 1 << position;
	}

	void bitfield_flip(bitfield * bitstream)
	{
		int i;
		for (i=0;i<sizeof(bitfield)*4;i++)
			bitfield_bit_flip(bitstream,i);
		
	}
	
	void bitfield_reverse(bitfield * bitstream)
	{
		bitfield temp_bitstream = * bitstream;
		bitfield mask = 1,i;

		for (i=0;i<sizeof(bitfield)*4;i++)
		{
			bitfield_bit_change(bitstream,sizeof(bitfield)*4-(i+1),(mask & temp_bitstream));
			mask = mask << 1;
		}		
	}