#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "extend.h"

char * num_to_str( double value )
{
	char * result;
	double temp_value;
	double prt_int, prt_frac;
	int cnt_fract = 0, cnt_int = 0, tmp_ivalue, ps_result = 0;

	temp_value = value;
	while ( ( prt_frac = modf( temp_value, &prt_int ) ) != 0 && cnt_fract++ < 6 )
		temp_value *= 10;

	temp_value = value;
	prt_frac = modf( temp_value, &prt_int );
	while ( prt_int != 0 )
		prt_frac = modf( ( temp_value / pow( 10, ++cnt_int ) ), &prt_int );

	cnt_int = cnt_int == 0 ? 1 : cnt_int;

	result = ( char * ) malloc( ( ( value < 0 ? 1 : 0 ) + cnt_int + ( cnt_fract == 0 ? 0 : cnt_fract + 1 ) + 1 ) * sizeof( char ) );

	*result = '-';
	ps_result = ( value < 0 ? 1 : 0 );

	value = fabs( value );

	prt_frac = modf( value, &prt_int );

	tmp_ivalue = ( int ) prt_int;

	while ( cnt_int-- != 0 )
	{
		result [ ps_result++ ] = ( ( tmp_ivalue - ( tmp_ivalue % ( int ) pow( 10, cnt_int ) ) ) / ( int ) pow( 10, cnt_int ) ) + 48;
		tmp_ivalue -= ( tmp_ivalue - ( tmp_ivalue % ( int ) pow( 10, cnt_int ) ) );
	}

	result [ ps_result ] = '.';
	ps_result += cnt_fract == 0 ? 0 : 1;

	tmp_ivalue = ( int ) ( prt_frac*pow( 10, cnt_fract ) + ( double ) 5 / pow( 10, cnt_fract ) );

	while ( cnt_fract-- != 0 )
	{
		result [ ps_result++ ] = ( ( tmp_ivalue - ( tmp_ivalue % ( int ) pow( 10, cnt_fract ) ) ) / ( int ) pow( 10, cnt_fract ) ) + 48;
		tmp_ivalue -= ( tmp_ivalue - ( tmp_ivalue % ( int ) pow( 10, cnt_fract ) ) );
	}

	result [ ps_result ] = 0;

	return result;
}
	
	double power (double X, int Y)
	{
		int i;
		double value = 1;
		for (i = 0; i < Y; i++)
		value *= X;
		return value;
	}
